Denizen brings a brand new type of urban living to Denver. Denizen is more than just a bed to rest your head. It's a community that is intimately connected to the eclectic South Broadway neighborhood and has a deep passion for all of Denver. Located just 30 steps from the Alameda Light Rail stop.
||
Address: 415 South Cherokee St, Denver, CO 80223, USA ||
Phone: 303-848-4773
